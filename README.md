# Estatística Básica (eBook)

<div align="justify">

<p><a target='_blank' rel='noopener noreferrer' href='https://statisticalmetrology.shinyapps.io/basicstat/'><b>Estatística Básica</b></a> é uma página web que contém, em caráter introdutório, conceitos básicos sobre análise estatística de dados e constitui material de apoio para cursos de programação em linguagem <a target='_blank' rel='noopener noreferrer' href='https://www.r-project.org/'><b>R</b></a> ou <a target='_blank' rel='noopener noreferrer' href='https://www.python.org/'><b>Python</b></a>. Para comentários, dúvidas e sugestões entre em contato por meio do correio eletrônico <a href="mailto:statistical.metrology@gmail.com"><b>statistical.metrology@gmail.com</b></a>.</p>

</div>

<div align="center"><img src="Estatistica_Basica.gif"/></div>

<p> <br /> </p>

# Nota

<div align="justify">

<p>A função [`ggbiplot.R`](ggbiplot.R) (<a target='_blank' rel='noopener noreferrer' href='https://github.com/vqv/ggbiplot'>GitHub</a>) foi desenvolvida pelo professor <a target='_blank' rel='noopener noreferrer' href='http://www.vince.vu/'>Vincent Q. Vu</a>.</p>

</div>


<p> <br /> </p>


