
# ========= Lindley distribution with parameter theta ========= #

            # References:
            # https://search.r-project.org/CRAN/refmans/VGAM/html/lindUC.html
            # https://www.rdocumentation.org/packages/LindleyR/versions/1.0.0/topics/Lindley


            # Install missing packages
              list_of_packages = c("lamW", "VGAM")
              ind = which(list_of_packages %in% installed.packages()[,"Package"] == FALSE)
              if(length(ind) == 0){"all packages installed"} else {install.packages(list_of_packages[ind])}








##############################
##############################
##############################
##############################
####                      ####
####                      ####
####   Density function   ####
####                      ####
####                      ####
##############################
##############################
##############################
##############################

# Function
dlindley <- function(x, theta, log = FALSE)
{
  stopifnot(theta > 0)
  if(log)
  {
    t1 <- log(theta)
    t4 <- log1p(theta)
    t6 <- log1p(x)
    -theta * x + 2 * t1 - t4 + t6
  }
  else
  {
    t1 <- theta ^ 2
    t7 <- exp(-theta * x)
    t1 / (1 + theta) * (1 + x) * t7
  }
}


# ========= Checking ========= #
            x = c(1, 6, 9, 16)
            fx1 = VGAM::dlind(x, theta = 5, log = FALSE)
            fx2 = dlindley(x, theta = 5)
            n = 16 ; round(fx1, digits = n) == round(fx2, digits = n)








###################################
###################################
###################################
###################################
####                           ####
####                           ####
####   Distribution function   ####
####                           ####
####                           ####
###################################
###################################
###################################
###################################

# Function
plindley <- function(q, theta, lower.tail = TRUE, log.p = FALSE)
{
  stopifnot(theta > 0)
  if(lower.tail)
  {
    t1  <- theta * q
    t6  <- exp(-t1)
    cdf <- 1 - (1 + t1 / (1 + theta)) * t6
  }
  else
  {
    t1  <- theta * q
    t6  <- exp(-t1)
    cdf <- (1 + t1 / (1 + theta)) * t6
  }
  if(log.p) return(log(cdf)) else return(cdf)
}


# ========= Checking ========= #
            q = c(0.1, 0.3, 0.4, 0.9)
            probabi1 = VGAM::plind(q, theta = 5)
            probabi2 = plindley(q, theta = 5)
            n = 15 ; round(probabi1, digits = n) == round(probabi2, digits = n)








###############################
###############################
###############################
###############################
####                       ####
####                       ####
####   Quantile function   ####
####                       ####
####                       ####
###############################
###############################
###############################
###############################

# Function
qlindley <- function(p, theta, lower.tail = TRUE, log.p = FALSE)
{
library(lamW)
  stopifnot(theta > 0)
  if(lower.tail)
  {
    t1  <- 1 + theta
    t4  <- exp(-t1)
    t6  <- lambertWm1(t1 * (p - 1) * t4)
    qtf <- -(t6 + 1 + theta) / theta
  }
  else
  {
    t1  <- 1 + theta
    t3  <- exp(-t1)
    t5  <- lambertWm1(-p * t1 * t3)
    qtf <- -(t5 + 1 + theta) / theta
  }
  if(log.p) return(log(qtf)) else return(qtf)
}


# ========= Results ========= # NOTE: THERE IS NO QUANTILE FUNCTION IN VGAM R PACKAGE.
            p = c(0.95, 0.975, 0.99)
            qlindley(p, theta = 5)








#######################################
#######################################
#######################################
#######################################
####                               ####
####                               ####
####   Random numbers generation   ####
####                               ####
####                               ####
#######################################
#######################################
#######################################
#######################################

# Function
rlindley <- function(n, theta, mixture = TRUE) # mixture: logical. If TRUE (default), random values are generated from a two-component mixture of gamma distributions, otherwise from the quantile function.
{
  stopifnot(theta > 0)
  if(mixture)
  {
    x <- rbinom(n, size = 1, prob = theta / (1 + theta))
    x * rgamma(n, shape = 1, rate = theta) + (1 - x) * rgamma(n, shape = 2, rate = theta)
  }
  else
  {
    qlindley(p = runif(n), theta, lower.tail = TRUE, log.p = FALSE)
  }
}



# ========= Results ========= # NOTE: DIFFERENTS PROCEDURES TO GENERATE RANDOM NUMBERS.
            set.seed(1234) ; r1 = rlindley(n = 20000, theta = 12)
            set.seed(1234) ; r2 = VGAM::rlind(n = 20000, theta = 12)
            par(mfrow = c(1, 2))
            hist(r1 , col = "#69b3a2" , main = "rlindley"    , xlab = "")
            hist(r2 , col = "#69b3a2" , main = "VGAM::rlind" , xlab = "")








#######################################################
#######################################################
#######################################################
#######################################################
####                                               ####
####                                               ####
####   Hazard rate function for theone-parameter   ####
####                                               ####
####                                               ####
#######################################################
#######################################################
#######################################################
#######################################################

# Function
hlindley <- function(x, theta, log = FALSE)
{
  stopifnot(theta > 0)
  if(log)
  {
    t1 <- log(theta)
    t5 <- log1p(theta * x + theta)
    t7 <- log1p(x)
    2 * t1 - t5 + t7
  }
  else
  {
    t1 <- theta ^ 2
    t1 / (theta * x + theta + 1) * (1 + x)
  }
}

