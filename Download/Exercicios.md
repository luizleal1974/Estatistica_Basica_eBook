<div align="justify">

# Lista de Exercícios

<p> <br /> </p>

<p>1. As taxas de retorno semanais para cinco ações listadas na Bolsa de Valores de Nova York foram determinadas para o período de janeiro de 2004 a dezembro de 2005. As ações pertencem a 3 empresas do setor bancário (JP Morgan, Citibank e Well Fargo) e duas do setor petrolífero (Royal Dutch Shell e Exxon Mobil). As taxas de retorno semanais são definidas como (preço de fechamento da semana atual - preço de fechamento da semana anterior)/(preço de fechamento da semana anterior), ajustado para desdobramentos de ações e dividendos.</p>

<p align="center"><b>[`stock price`](Dados_Exercicios/stock_price.xlsx)</b></p>

<p>a) Construa um: (i) <b>gráfico de linhas</b> que expresse a evolução dos preços das ações das empresas JP Morgan, Citibank, Well Fargo, Royal Dutch Shell e Exxon Mobil durante as semanas; (ii) <b>histograma</b> para o preço das ações da empresa Exxon Mobil; (iii) único gráfico que contenha, para cada uma das cinco empresas, o <b>boxplot</b> dos dados; (iv) gráfico de <b>barra de erro</b>.</p>

<p>b) Considerando os dados referentes aos preços das ações das empresas JP Morgan, Citibank, Well Fargo, Royal Dutch Shell e Exxon Mobil realize uma: (i) análise de componentes principais (<b>PCA</b>); (ii) <b>análise fatorial</b> sem rotação das cargas fatoriais (o método de estimação das cargas fatoriais pode ser componentes principais ou máxima verossimilhança).</p>


<p> <br /> </p>


<p>2. Os dados contêm variáveis utilizadas para comparar ou classificar as principais universidades americanas. Essas variáveis incluem SAT (pontuação média no SAT dos calouros), Top10 (porcentagem dos calouros nos 10% melhores da classe no ensino médio), Accept (porcentagem de candidatos aceitos), SFRatio (razão discente-docente), Expenses (despesas anuais estimadas) e Grad (taxa de graduação). O arquivo de dados abrange 25 universidades americanas e, no que tange a codificação, cabe destacar que Cal_Tech é <i>California Institute of Technology</i>, Penn_State é <i>Pennsylvania State University</i> e U_Penn é <i>University of Pennsylvania</i>. Por fim, SAT é o teste de aptidão escolar (<i>Scholastic Aptitude Test</i>) é um dos exames mais comuns dos EUA, utilizado pelas universidades estadunidenses em seus processos de admissão para graduação.</p>

<p align="center"><b>[`Universities`](Dados_Exercicios/Universities.xlsx)</b></p>

<p>a) Utilize a variável <i>Type</i> para construir um: (i) <b>gráfico de barras</b>; (ii) <b>gráfico de setores</b>.</p>

<p>b) Utilize <b>análise de cluster</b> e escalonamento multidimensional (<b>MDS</b>) para tentar identificar como as universidades se agrupam. Para o estudo selecione o banco de dados com as seguintes variáveis: <i>SAT</i>, <i>Top10</i>, <i>Accept</i>, <i>SFRatio</i>, <i>Expenses</i> e <i>Grad</i>.</p>


<p> <br /> </p>


<p>3. As condições ideais para extrusão de filme plástico foram examinadas usando uma técnica chamada <i>Operação Evolutiva</i>. No decorrer do estudo que foi feito, três respostas (resistência ao rasgo, brilho e opacidade) foram medidas em dois níveis dos fatores (mudança na taxa de extrusão e quantidade de aditivo). O fator <i>mudança na taxa de extrusão</i> foi avaliado em dois níveis: baixo (-10%) e alto (10%). O fator <i>quantidade de aditivo</i> também foi investigado em dois níveis: baixo (1,0%) e alto (1,5%). As medições foram repetidas n = 5 vezes em cada combinação dos níveis dos fatores.</p>

<p align="center"><b>[`plastic film`](Dados_Exercicios/plastic_film.xlsx)</b></p>

<p>a) Construa um único gráfico que contenha, para cada uma das respostas (resistência ao rasgo, brilho e opacidade), o <b>boxplot</b> dos dados segmentados pelo fator <i>quantidade de aditivo</i>.</p>

<p>b) Verifique se a <i>mudança na taxa de extrusão</i>, a <i>quantidade de aditivo</i> e a interação, de ambas, afetam as respostas (resistência ao rasgo, brilho e opacidade). Na linguagem de programação R utilize [`type = "II"`](Download/Exercicio_3.pdf). (<b>Manova: DoE</b>)</p>

<p>c) Verifique se existe relação estatisticamente significativa entre as variáveis resposta (y1 e y2) e a variável preditora x (Tabela 1). (<b>Manova: Modelos lineares</b>)</p>

<p align="center">
<img src="Dados_Exercicios/Tabela1.png" alt="Drawing">
</p>


<p> <br /> </p>


<p>4. Uma empresa têxtil tece um tecido em um grande número de teares. É desejável que os teares sejam homogêneos para que se possa obter um tecido de resistência uniforme. Existe a suspeita de que, além da variação usual na resistência dentro de amostras de tecido do mesmo tear, podem haver variações significativas na resistência entre os teares. Para investigar isso, selecionou-se quatro teares aleatoriamente e foram realizadas quatro determinações de resistência no tecido fabricado em cada tear. Este experimento foi executado em ordem aleatória.</p>

<p align="center"><b>[`resistencia`](Dados_Exercicios/resistencia.xlsx)</b></p>

<p>a) Verifique se há variações significativas na resistência do tecido entre os teares.</p>


<p> <br /> </p>


<p>5. Atividade física é qualquer movimento voluntário produzido pela musculatura que resulte num gasto de energia acima do nível de repouso. O arquivo de dados contém informações concernentes à atividade física de 162 pessoas e é composto de quatro (4) variáveis: duração, em minutos, da atividade física (<i>Duration</i>), pulsação média (<i>Pulse</i>), pulsação máxima (<i>Maxpulse</i>) e caloria gasta (<i>Calories</i>).</p>

<p align="center"><b>[`workout`](Dados_Exercicios/workout.xlsx)</b></p>

<p>a) Construa um <b>scatter plot</b> com as variáveis <i>Duration</i> (x) e <i>Calories</i> (y).</p>


<p> <br /> </p>


<p>6. O conjunto de dados é constituído de 4 variáveis meteorológicas (direção, temperatura, umidade e velocidade) medidas em 4 bairros do município do Rio de Janeiro: Botafogo (Urca), Copacabana, Gericinó e Lagoa. As medições foram realizadas no período de 01 de janeiro de 2016 a 05 de novembro de 2016. Os dados foram segmentados de acordo com as 4 estações do ano (verão, outono, inverno e primavera).</p>

<p align="center"><b>[`Meteorologia`](Dados_Exercicios/Meteorologia.xlsx)</b></p>

<p>a) Verifique se a estação do ano, o bairro e a interação, de ambos, afetam as respostas (direção, temperatura, umidade e velocidade).</p>


<p> <br /> </p>


<p>7. O arquivo de dados contém a avaliação de 1728 automóveis. Os veículos estão enquadrados nas seguintes categorias: inaceitável (<i>unacc</i>), aceitável (<i>acc</i>), bom (<i>good</i>) e muito bom (<i>vgood</i>). Os carros foram avaliados por meio das seguintes características: preço de compra (<i>buying</i>), custo de manutenção (<i>maint</i>), número de portas (<i>doors</i>), capacidade (<i>persons</i>), tamanho do porta-malas (<i>lug_boot</i>) e segurança estimada do carro (<i>safety</i>). A capacidade de um veículo é a quantidade de pessoas que o veículo consegue transportar, incluído nessa contagem o condutor.</p>

<p align="center"><b>[`car evaluation`](Dados_Exercicios/car_evaluation.xlsx)</b></p>

<p>a) Escolha uma técnica (<b>SVM</b>, <b>redes neurais</b>, <b>k-NN</b> ou <b>RFC</b>) para classificar a variável <i>Class</i>.</p>


<p> <br /> </p>


<p>8. A resistência à compressão do concreto é conhecida como Fck (<i>Feature Compression Know</i>) e é medida em megapascal (mpa), em que cada 1 mpa corresponde a uma resistência aproximada de 10 kgf/cm<sup>2</sup>. O Fck indica, portanto, a qual tensão o concreto tem capacidade de resistir. Essa tensão é a resultante da divisão entre a força e a área em que ela atuará. Dessa forma, os testes de resistência no concreto possibilitam confirmar a tensão máxima a que ele resistirá antes de sofrer ruptura. Os dados (hipotéticos) referem-se à Fck de 150 amostras de concreto.</p>

<p align="center"><b>[`concreto`](Dados_Exercicios/concreto.xlsx)</b></p>

<p>a) Utilize o <b>Q-Q plot</b> e o teste de <b>Anderson-Darling</b> para verificar a aderência dos dados à distribuição de Laplace (dupla exponencial).</p>

<p> <br /> </p>


<p> 9.  Num experimento visando ao controle do pulgão (<i>Aphis gossypii Glover</i>) em cultura de pepino, utilizou-se 6 repetições dos seguintes tratamentos: Testemunha; Azinfós etílico; Supracid 40CE dose 1; Supracid 40CE dose 2; Diazinon 60CE (Tabela 2). O delineamento experimental adotado foi inteiramente ao acaso e os dados obtidos são referentes ao número de pulgões coletados 36 horas após a pulverização.</p>

<p align="center">
<img src="Dados_Exercicios/Tabela2.png" alt="Drawing">
</p>

<p align="center"><b>[`pulgoes`](Dados_Exercicios/pulgoes.xlsx)</b></p>

<p>a) Construa um único gráfico que contenha, para cada um dos tratamentos, o <b>boxplot</b> dos dados.</p>

<p>b) Verifique, por meio do teste de <b>Kruskal-Wallis</b>, se há variações significativas no número de pulgões entre os tratamentos.</p>

<p> <br /> </p>


<p> 10.  Este arquivo de dados contém informações sobre as variáveis <i>velocity ratio</i> (x) e %Err (y) para 5 tipos de óleos: <i>gas</i>, <i>kerosene</i>, <i>siptech 34 cSt</i>, <i>siptech 46 cSt</i> e <i>siptech 90 cSt</i>.</p>

<p align="center"><b>[`oil`](Dados_Exercicios/oil.xlsx)</b></p>

<p>a) Verifique se o tipo de óleo influencia nas medições.</p>


<p> <br /> </p>

# Referência

[1] Johnson and Wichern, Applied Multivariate Statistical Analysis, 6th Ed, Pearson, 2007.

[2] Douglas C. Montgomery, Design and Analysis of Experiments, 10th Edition, John Wiley & Sons, 2020.

[3] Banzatto e Kronka, Experimentação agrícola, 4<sup><u>a</u></sup> Edição, Funep, 2006.


</div>

