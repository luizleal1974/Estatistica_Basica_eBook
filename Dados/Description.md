<div align="justify">

# Dados

Esta pasta contém arquivos de dados que podem ser utilizados para a prática de programação nas linguagens R e Python. <b><a target='_blank' rel='noopener noreferrer' href='https://www.r-project.org/'>R</a></b> é um software livre para computação estatística e construção de gráficos. <b><a target='_blank' rel='noopener noreferrer' href='https://www.python.org/'>Python</a></b> é uma linguagem de programação de uso geral. Os conjuntos de dados contidos nesta pasta não foram contemplados no corpo do texto do eBook <b>Estatística Básica</b> nem na lista de exercícios do referido livro eletrônico. As bases de dados estão disponíveis em arquivos Microsoft &reg; Excel &reg; com extensão xlsx.

<p> <br /> <p>

# 1. Fuel Consumption

<b>Arquivo:</b> [Fuel_Consumption.xlsx](Dados/Fuel_Consumption.xlsx)

<b>Descrição:</b> Os conjuntos de dados fornecem classificações de consumo de combustível específicas do modelo e emissões estimadas de dióxido de carbono para veículos leves novos para venda no varejo no Canadá.

<b>Sugestão de análise:</b> Gráficos de setores ou barras, tabela de contingência, boxplot, densidade kernel, teste de Shapiro-Wilk, modelo de regressão linear simples e múltipla.

<b>Fonte:</b> <a target='_blank' rel='noopener noreferrer' href='https://open.canada.ca/data/en/dataset/98f1a129-f628-4ce4-b24d-6f16bf24dd64'>Government of Canada</a>

<p> <br /> <p>

# 2. Social network advertising

<b>Arquivo:</b> [Social_Network_Ads.xlsx](Dados/Social_Network_Ads.xlsx)

<b>Descrição:</b> O arquivo de dados contém informações sobre a decisão de compra de um produto com base no sexo, idade e salário estimado.

<b>Sugestão de análise:</b> Gráficos de setores ou barras, tabela de contingência, boxplot, densidade kernel e regressão logística. Modelos SVM, redes neurais, k-NN ou RFC para classificar a variável `Purchased` (decisão de compra) a partir do sexo, idade e salário estimado. Nos modelos de classificação (SVM, redes neurais, k-NN ou RFC) sugere-se converter a variável categórica `sexo` em variável numérica.

<p> <br /> <p>

# 3. Bio

<b>Arquivo:</b> [bio.xlsx](Dados/bio.xlsx)

<b>Descrição:</b> Os dados referem-se à uma comparação interlaboratorial (CI) para avaliar a eficácia da metabolômica RMN <sup>1</sup>H em gerar conjuntos de dados comparáveis a partir de amostras derivadas do meio ambiente. Sete laboratórios dos Estados Unidos, um do Canadá, um do Reino Unido e um da Austrália participaram da intercomparação. Analisaram-se misturas de metabólitos sintéticos e amostras de origem biológica de extratos de fígado de linguado europeu de locais limpos e contaminados. O linguado europeu adulto feminino (Platichthys flesus) foi coletado na foz dos rios Alde (local de controle não poluído) e Tyne (local poluído) no Reino Unido. Cada participante forneceu como resultado um espectro por RMN <sup>1</sup>H. Os espectros foram reportados com deslocamento químico variando de 10 a 0,2 partes por milhão (ppm).

<b>Sugestão de análise:</b> Gráfico de linhas e escalonamento multidimensional.

[![](https://img.shields.io/badge/doi-10.1016/j.chemolab.2016.12.010-yellow.svg)](https://doi.org/10.1016/j.chemolab.2016.12.010)

<p> <br /> <p>

# 4. Salmonella

<b>Arquivo:</b> [salmonella.xlsx](Dados/salmonella.xlsx)

<b>Descrição:</b> O <a target='_blank' rel='noopener noreferrer' href='https://www.izsvenezie.com'>Istituto Zooprofilattico Sperimentale delle Venezie</a> conduziu um ensaio de proficiência em que 19 sorotipos de salmonela foram enviados a 12 participantes. O objetivo do ensaio era verificar a capacidade do laboratório participante de identificar corretamente o sorotipo enviado.

<b>Sugestão de análise:</b> Utilizar o coeficiente kappa para avaliar o grau de concordância entre o laboratório 6 (`Lab 6`) e o resultado esperado (`Expected result`) fornecido pelo provedor do ensaio de proficiência.

[![](https://img.shields.io/badge/doi-10.1007/s00769.015.1129.0-yellow.svg)](http://dx.doi.org/10.1007/s00769-015-1129-0)

<p> <br /> <p>

# 5. Wine

<b>Arquivo:</b> [wine.xlsx](Dados/wine.xlsx)

<b>Descrição:</b> Os dados são provenientes de uma análise química de vinhos cultivados na mesma região da Itália, mas derivados de três cultivares diferentes. A análise determinou as quantidades de 13 constituintes encontrados em cada um dos três tipos de vinhos.

<b>Sugestão de análise:</b> Modelos SVM, redes neurais, k-NN ou RFC para classificar a variável `target` (tipo de vinho) a partir dos 13 constituintes.

<b>Fonte:</b> 
<a target='_blank' rel='noopener noreferrer' href='http://archive.ics.uci.edu/ml/datasets/Wine'>UCI Machine Learning Repository</a>

<p> <br /> <p>

# 6. Energy

<b>Arquivo:</b> [Energy.xlsx](Dados/Energy.xlsx)

<b>Descrição:</b> Realizaram-se análises de eficiência energética utilizando 12 diferentes formatos de edifícios simulados no Ecotect. Os edifícios diferem no que diz respeito à área envidraçada, à distribuição da área envidraçada e à orientação, entre outros parâmetros. Foram simuladas diversas configurações em função das características mencionadas obtendo-se 768 formas de edifícios. O conjunto de dados contém oito atributos (ou recursos) denotados por: compacidade relativa (X1), área de superfície (X2), área da parede (X3), área do telhado (X4), altura geral (X5), orientação (X6), área de envidraçamento (X7) e distribuição da área de envidraçamento (X8). Além destes recursos, o arquivo de dados possui duas variáveis resposta (ou resultados) denotadas por: carga de aquecimento (Y1) e carga de resfriamento (Y2).

<b>Sugestão de análise:</b> Manova.

<b>Fonte:</b> 
<a target='_blank' rel='noopener noreferrer' href='https://archive.ics.uci.edu/ml/datasets/Energy+efficiency'>UCI Machine Learning Repository</a>
<p> <br /> <p>

</div>
