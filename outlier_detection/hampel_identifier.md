# 1. Hampel identifier

### 1.1 Conjunto de dados: INSTAB

<div align="justify">

<p><a target='_blank' rel='noopener noreferrer' href='https://doi.org/10.1016/S0959-1524(00)00046-9'><code>Pearson (2001)</code></a> lista em seu artigo (página 187), por meio do identificador de Hampel, os valores suspeitos de serem outliers no conjunto de dados INSTAB: 2<sup>a</sup>, 6<sup>a</sup>, 7<sup>a</sup>, 17<sup>a</sup>, 18<sup>a</sup> e 19<sup>a</sup> observações.</p>

```
# Load functions
path = "https://gitlab.com/luizleal1974/Estatistica_Basica_eBook/raw/main/outlier_detection/hampel_identifier.R"
devtools::source_url(path)

# Hampel identifier
which(INSTAB %in% hampel_identifier(INSTAB))
```
</div>

<p><br></p>

### 1.2 Conjunto de dados: SNE

<div align="justify">

<p>No que tange o conjunto de dados SNE, <a target='_blank' rel='noopener noreferrer' href='https://doi.org/10.1016/S0959-1524(00)00046-9'><code>Pearson (2001)</code></a> identifica a 2<sup>a</sup> observação como sendo suspeita de ser um outlier. Entretanto, ao se utilizar o identificador de Hampel descrito neste <a target='_blank' rel='noopener noreferrer' href='https://gitlab.com/luizleal1974/Estatistica_Basica_eBook/-/blob/main/outlier_detection/outlier_detection.md'><code>repositório</code></a>, nenhuma observação é identificada como outlier.</b>

Em ambos os casos, a i-ésima observação $x_i$ é considerada um outlier se:

&#x2022; <a target='_blank' rel='noopener noreferrer' href='https://doi.org/10.1016/S0959-1524(00)00046-9'><code>Pearson (2001)</code></a>: $\large \vert x_i - median(\mathbf{x})\vert \gt 1.4826 \cdot MAD(\mathbf{x})$

&#x2022; <a target='_blank' rel='noopener noreferrer' href='https://gitlab.com/luizleal1974/Estatistica_Basica_eBook/-/blob/main/outlier_detection/outlier_detection.md'><code>Repositório</code></a>: $\large \vert x_i - median(\mathbf{x})\vert \ge g(n, \alpha_n) \cdot MAD(\mathbf{x})$
</p>

```
# Load functions
path = "https://gitlab.com/luizleal1974/Estatistica_Basica_eBook/raw/main/outlier_detection/hampel_identifier.R"
devtools::source_url(path)

# Pearson (2001)
n = length(SNE)
MAD = function(x) median(abs(x - median(x)))
dk = abs(SNE - median(SNE)) / (1.4826 * MAD(x = SNE))
which(dk > 3)

# Hampel identifier
hampel_identifier(SNE)
```
</div>


<p><br></p>


# 2. Comparando métodos

### 2.1 Conjunto de dados: INSTAB

<div align="justify">

<p>No conjunto de dados INSTAB (Figura 1), os métodos de <b>z-score modificado, GESD e Hampel</b> separam, de modo <b>mais eficiente</b>, observações regulares de outliers quando comparados a outros métodos (<a target='_blank' rel='noopener noreferrer' href='https://gitlab.com/luizleal1974/Estatistica_Basica_eBook/blob/main/outlier_detection/Comparing_methods.R'><code>Comparing_methods.R</code></a>).</p>

<div align="center"><img src="outlier_detection/INSTAB.png" height="350" width="550"/></div>

<div align="center">Figura 1. Conjunto de dados: INSTAB (<a target='_blank' rel='noopener noreferrer' href='https://doi.org/10.1016/S0959-1524(00)00046-9'><code>Pearson, 2001</code></a>).</div>

<p></p>

```
# Comparando métodos
path = "https://gitlab.com/luizleal1974/Estatistica_Basica_eBook/raw/main/outlier_detection/Comparing_methods.R"
result = devtools::source_url(path)
result
```

<div align="center"><img src="outlier_detection/Comparing_methods.png" height="450" width="550"/></div>


<code>Nota: Nos scores (z-score, <b>z-score modficado</b>, t-score, qui-score e IQR-score) apenas o <b>z-score modficado</b>, relacionado na tabela acima, idenficou outliers (<a target='_blank' rel='noopener noreferrer' href='https://gitlab.com/luizleal1974/Estatistica_Basica_eBook/blob/main/outlier_detection/Comparing_methods.R'><code>Comparing_methods.R</code></a>).</code>

</div>


<p><br></p>

### 2.2 Conjunto de dados: ANVO

<div align="justify">

<p>No conjunto de dados ANVO (anidro volumétrico), os métodos de <b>Chauvenet, z-score, t-score, qui-score e GESD</b> separam, de modo <b>mais eficiente</b>, observações regulares de outliers quando comparados a outros métodos (<a target='_blank' rel='noopener noreferrer' href='https://gitlab.com/luizleal1974/Estatistica_Basica_eBook/blob/main/outlier_detection/Comparing_methods_2.R'><code>Comparing_methods_2.R</code></a>). Acesse <a target='_blank' rel='noopener noreferrer' href='https://colab.research.google.com/drive/1Da5q6RsEyAvqkm4ZeVe66a3Wv2OLg3_g?usp=sharing'><code>Google Colab</code></a>.</p>

```
# Comparando métodos
path = "https://gitlab.com/luizleal1974/Estatistica_Basica_eBook/raw/main/outlier_detection/Comparing_methods_2.R"
devtools::source_url(path)
final_result[c(1, 6, 8, 9, 11),]
```

<div align="center"><img src="outlier_detection/Comparing_methods_2.png" height="310" width="410"/></div>


</div>


<p><br></p>

