# 1. Hampel

<div align="justify">

<p>O identificador de Hampel (<a target='_blank' rel='noopener noreferrer' href='https://doi.org/10.1080/01621459.1974.10482962'><code>Hampel, 1974</code></a>) é um método robusto de identificação de outliers. O método é recomendado para dados assimétricos (<a target='_blank' rel='noopener noreferrer' href='https://doi.org/10.1109/87.974338'><code>Pearson, 2002</code></a>) e pequenas amostras (<a target='_blank' rel='noopener noreferrer' href='https://doi.org/10.1016/S0959-1524(00)00046-9'><code>Pearson, 2001</code></a>). Nesta abordagem, se

$\large \vert x_i - median(\mathbf{x})\vert \ge g(n, \alpha_n) \cdot MAD(\mathbf{x})$

Conclui-se que a i-ésima observação $x_i$ é um outlier (<a target='_blank' rel='noopener noreferrer' href='https://doi.org/10.1016/j.compchemeng.2004.01.009'><code>Liu <i>et al.</i>, 2004</code></a>). Cabe destacar que $\mathbf{x} = (x_1, x_2, \cdots, x_n)$ e

$\large MAD(\mathbf{x}) = mediana(\vert x_i - mediana(\mathbf{x}) \vert)$

Os valores de $g(n, \alpha_n)$ são obtidos conforme descrito em <a target='_blank' rel='noopener noreferrer' href='https://doi.org/10.2307/2290763'><code>Davies and Gather (1993)</code></a>. O tamanho amostral mínimo para utilizar a função <a target='_blank' rel='noopener noreferrer' href='https://gitlab.com/luizleal1974/Estatistica_Basica_eBook/blob/main/outlier_detection/hampel_identifier.R'><code>hampel_identifier.R</code></a> depende do parâmetro $\alpha$ escolhido (Tabela 1).

<div align="center"><img src="outlier_detection/Tabela_1.png"/></div>

No arquivo <a target='_blank' rel='noopener noreferrer' href='https://gitlab.com/luizleal1974/Estatistica_Basica_eBook/blob/main/outlier_detection/hampel_identifier.md'><code>hampel_identifier.md</code></a> há uma análise complementar sobre o método. Por fim, é importante destacar que a <b>quantidade de outliers detectados pode variar</b> conforme os argumentos selecionados (Figura 1).  
</p>

```
# Load functions
path = "https://gitlab.com/luizleal1974/Estatistica_Basica_eBook/raw/main/outlier_detection/hampel_identifier.R"
devtools::source_url(path)

# Hampel identifier
x = c(0.2, 0.31, 1.19, 1.21, 1.24, 1.25, 1.27, 1.29, 1.32, 1.34, 1.36, 1.37, 1.39, 1.43, 1.45, 1.48, 1.49, 1.5, 1.6, 1.8, 2.0, 2.1, 2.2, 2.3, 2.4, 2.6, 2.8)
hampel_identifier(x)                            # Outliers
x[-which(x %in% hampel_identifier(x))]          # Dados sem outliers
plot_hampel(x, arg_order = c(1, 5, 3, 2, 6, 4)) # Plot
```

</div>



<div align="center"><img src="outlier_detection/hampel_identifier.png"/></div>



<div align="center">Figura 1. Identificador de Hampel.</div>






<p><br></p>






# 2. Regra da mediana

<div align="justify">

<p>A regra de mediana (<a target='_blank' rel='noopener noreferrer' href='https://gitlab.com/luizleal1974/Estatistica_Basica_eBook/blob/main/outlier_detection/median_rule.R'><code>median_rule.R</code></a>) é um método de detecção de outliers para dados não-gaussianos. Nesta metodologia, a detecção de outliers é pouco afetada pelo tamanho amostral e, neste contexto, o método é recomendado para pequenas amostras. Esta abordagem fornece bons resultados para dados que apresentem distribuição moderadamente assimétrica (<a target='_blank' rel='noopener noreferrer' href='http://dx.doi.org/10.1016/S0167-9473(99)00057-2'><code>Carling, 2000</code></a>).

Seja $\mathbf{x} = (x_1, x_2, \cdots, x_n)$. A i-ésima observação $x_i$ é considerada um outlier se

$\large x_i > mediana(\mathbf{x}) + k \cdot IQR(\mathbf{x})$

Em que $IQR(\mathbf{x})$ é o intervalo interquartílico e

$\large k = \Large \frac{17.63\cdot n - 23.64}{7.74\cdot n - 3.71}$

```
# Load functions
path = "https://gitlab.com/luizleal1974/Estatistica_Basica_eBook/raw/main/outlier_detection/median_rule.R"
devtools::source_url(path)

# Regra da mediana
x = c(0.2, 0.31, 1.19, 1.21, 1.24, 1.25, 1.27, 1.29, 1.32, 1.34, 1.36, 1.37, 1.39, 1.43, 1.45, 1.48, 1.49, 1.5, 1.6, 1.8, 2.0, 2.1, 2.2, 2.3, 2.4, 2.6, 2.8)
median_rule(x)                                        # Outliers
x[-which(x %in% median_rule(x))]                      # Dados sem outliers
```
</p>

</div>







<p><br></p>








# 3. Tukey (boxplot) ajustado

<div align="justify">

<p>De acordo com <a target='_blank' rel='noopener noreferrer' href='https://doi.org/10.1016/j.csda.2007.11.008'><code>Hubert and Vandervieren (2008)</code></a>, a <b>regra da mediana</b> apresenta como desvantagem o fato de que o método não depende apenas do tamanho amostral, mas de algumas características da distribuição dos dados. Na <b>regra da mediana</b>, o critério de detecção de outliers é obtido a partir da classe de distribuições lambda generalizada que são caracterizadas por um parâmetro de locação, um parâmetro de escala e dois parâmetros de forma. Os autores afirmam que não está claro: (i) como o método funciona quando os parâmetros de forma precisam ser estimados; (ii) como o critério de detecção de outliers se aplica a outras distribuições.

Segundo <a target='_blank' rel='noopener noreferrer' href='https://doi.org/10.1016/j.csda.2007.11.008'><code>Hubert and Vandervieren (2008)</code></a>, o boxplot ajustado (<a target='_blank' rel='noopener noreferrer' href='https://search.r-project.org/CRAN/refmans/robustbase/html/adjbox.html'><code><b>adjbox</b></code></a>) possui as seguintes vantagens: (i) não é necessário fazer qualquer suposição sobre a distribuição dos dados; (ii) em distribuições assimétricas o método faz uma melhor distinção entre observações regulares e outliers.

```
# Load packages
library(robustbase)

# Tukey (boxplot) ajustado
x = c(0.2, 0.31, 1.19, 1.21, 1.24, 1.25, 1.27, 1.29, 1.32, 1.34, 1.36, 1.37, 1.39, 1.43, 1.45, 1.48, 1.49, 1.5, 1.6, 1.8, 2.0, 2.1, 2.2, 2.3, 2.4, 2.6, 2.8)
adjbox(x, range = 1.5, plot = FALSE)$out                   # Outliers
x[-which(x %in% adjbox(x, range = 1.5, plot = FALSE)$out)] # Dados sem outliers
```
</p>


</div>


<p><br></p>




